set expandtab                   " insert spaces for tab
set tabstop=4                   " use 4 spaces to display tabs
set softtabstop=4               " backspace deletes 4 spaces
set smarttab                    " use shiftwidth at start of lines

set shiftwidth=4                " autoindent 4 spaces
set autoindent                  " use autoindent
set copyindent                  " use indent of existing lines
set shiftround                  " indent to nearest tabstop
set backspace=indent,eol,start  " backspacing over autoindent, line breaks and start of inserts

set number                      " show line numbers
set linebreak                   " wrap lines
" TODO: fix scrolling through
"nnoremap k gk                   " navigate up through wrapped lines
"nnoremap j gj                   " navigate down through wrapped lines

set showmatch                   " show matching brackets
set hlsearch                    " highlight search results
set ignorecase                  " ignore case in search pattern
set smartcase                   " case sensitive search if pattern is not all lower case
set incsearch                   " show results as you type

set history=1000                " remember commands and search
set undolevels=1000             " log operations
set nobackup                    " disable backup file
set noswapfile                  " disable swap file

syntax on                       " syntax highlighting
set t_Co=256                    " 256 colors
colorscheme solarized           " solarized theme

set nocompatible                " turn off vi
filetype plugin on              " plugins via filetype
